## documentation/scripts for LPJ-GUESS forest age initialization with HILDA+ and forest age obs data.

`forest_age_init1.ipynb` contains a step by step documentation of the process and some examples of how to create LPJ-GUESS input files for some example applications. The file is a jupyter notebook file, you can run it interactively with e.g. jupyter-lab. If you don't have an environment to run jupyter notebooks, you can just watch open it in the gitlab browser, but there you can't execute the code interactively. All code is written in python.

`age_init.py` and `gross_st_matrix_hildaplus.py` are python scripts that contain functions that are used in the process.


*Martin Wittenbrink - martin.wittenbrink@kit.edu*

import numpy as np
import rasterio 
import netCDF4 as nc


def extend_hildaslice(slice1,years):
    ### function to extend extracted hilda+ gridcell area data back to
    ### extended with NATURAL except for BARREN pixels
    extend=np.repeat(5,slice1.shape[1]*slice1.shape[2]).reshape(slice1.shape[1],slice1.shape[2])
    extend[slice1[0]==6]=6
    return np.concatenate(([extend for i in range(years)],slice1))

def allnatural(slice1):
    ### convert all managed forest into NATURAL
    slc=np.copy(slice1)
    slc[slice1>=7]=5
    return slc

def allforest(slice1):
    ### convert all NATURAL into ForestPNV
    slc=np.copy(slice1)
    slc[slice1==5]=7
    return slc

def initial_nat_forest_split(slice1):
    ### initial split between FOREST and NATURAL by making all forest NATURAL in the beginning and every forest after LUC FOREST
    st3=allforest(slice1)
    natpx=[st3[0]>=7]
    for i in range(1,len(st3)):
        natpx.append((st3[i]==st3[i-1])*natpx[i-1])
    st3[natpx]=5
    return st3

def agestruct_coarsepixelhilda(hildadata1,year,hildayear0):
    ### calculate age of managed forest
    hildadata=hildadata1[:(year-hildayear0+1),:,:].copy()
    zs=np.zeros(hildadata.shape)
    zs[hildadata-hildadata[-1,:,:]!=0] = np.repeat( np.arange( hildadata.shape[0]),hildadata.shape[1]*hildadata.shape[2] ).reshape(hildadata.shape[0], hildadata.shape[1], hildadata.shape[2])[hildadata-hildadata[-1,:,:]!=0]
    return ((year-hildayear0)-np.max(zs,axis=0))[hildadata[-1,:,:]>=7]


def agestruct_coarsepixelhilda2(hildadata1,year,hildayear0):
    hildadata=hildadata1[:(year-hildayear0+1),:,:].copy()
    zs=np.zeros(hildadata.shape)
    zs[hildadata-hildadata[-1,:,:]!=0]=np.repeat(np.arange(hildadata.shape[0]),hildadata.shape[1]*hildadata.shape[2]).reshape(hildadata.shape[0],hildadata.shape[1],hildadata.shape[2])[hildadata-hildadata[-1,:,:]!=0]
    return ((year-hildayear0)-np.max(zs,axis=0))*(hildadata[-1,:,:]>=7)


def agestr_from_hildadata(slice1,dist_int,forestageyear,hildayear0):
    agestr_nat=agestr_natural(dist_int)
    agestr_for=agestruct_coarsepixelhilda(slice1,forestageyear,hildayear0)-1
    return np.histogram(agestr_for,bins=np.arange(0,170,20))[0]/(slice1.shape[1]*slice1.shape[2])  + agestr_nat*np.mean(slice1[forestageyear-hildayear0-1]==5)
    
def calc_harvest_experimental(agestr_lpjg,forestage_obs,forestageyear,natfrac,hildayear0):
    diff=agestr_lpjg-forestage_obs   ### calculate difference between lpjg and obs age structure
    harvest=np.zeros_like(diff)
    harvest_amount=diff[-1]/natfrac[-1]   ### maximum harvest amount to distribute is the difference in old growth forest divided by the fraction of old growth in natural forest
    harv_amou0=harvest_amount
    ran=np.arange(7)       # 7 age classes below old grwoth to be randomly shuffled
    np.random.shuffle(ran) # to randomly distribute the maximum harvest amount
    for k in ran:       ### calculate the harvest fractions for each of the 7 age classes
        if harvest_amount>0.:
            if diff[k]<0.:
                harv_here=np.min((harvest_amount,-1*diff[k] + (harv_amou0)*natfrac[k]))  ### harvest per age class is the difference per ageclass + the fraction of natural in that ageclass that is lost
                harvest[k]=harv_here
                harvest_amount-=harv_here
    harvest_years=np.zeros(forestageyear-hildayear0) 
    for k in range(7): ### place harvest at random years in resp. 20-years period
        kyear=-k*20-np.random.randint(20)  
        if kyear < -len(harvest_years):
            kyear=0
        harvest_years[kyear]+=harvest[k]
    return harvest_years    
    
    
def apply_harvest(slice1,harv_years,allforestslice,forestageyear,hildayear0):
    ### apply harvest fractions to hilda+ pixel data
    allforestage=agestruct_coarsepixelhilda2(allforestslice,forestageyear,hildayear0)
    st5=slice1.copy()
    haye=(harv_years*np.product(slice1.shape[1:])).astype(int)
    for year in range(len(haye)):
        if haye[year]>0:
            cells_avail=np.sum(st5[forestageyear-hildayear0-1]==5)
            for cell in range(np.min([haye[year],cells_avail])):
                agestr_for2=allforestage*(st5[forestageyear-hildayear0-1]==5)
                idx=np.where(agestr_for2==np.max(agestr_for2))[0][0],np.where(agestr_for2==np.max(agestr_for2))[1][0]
                st5[year:,idx[0],idx[1]]=allforestslice[year:,idx[0],idx[1]]
    return st5

### Pucher forest age data

#pucher=np.array([rasterio.open(f'C:\\forest_age\\pucher_age\\Age\\agecl_{i+1}_perc.tif').read()[0] for i in range(8)])
#lola_pucher=np.load("..\\lonlat_pucher.npy") #I computed the coordinates of the geotiff files somewhere and saved them...

#function to return pucher data for any given latitude, longitude and resolution
#def return_pucher_mask(lat,lon,res,lola_pucher):
#    return (lola_pucher[0]>=lon-0.5*res)*(lola_pucher[0]<lon+0.5*res)*(lola_pucher[1]>=lat-0.5*res)*(lola_pucher[1]<lat+0.5*res)

#forest_cover=np.load("forest_cover_puchergrid.npy")
#forest_cover=np.load("forest_cover_incshrubs_puchergrid.npy")

#pucher_forestcoverscaled=np.ma.masked_equal(pucher,-9999)
#for i in range(8):
#    puchi=pucher[i]>0.
#    pucher_forestcoverscaled[i,puchi]=pucher[i,puchi]*forest_cover[puchi]
    
#def pucher_forestage_gc(lat,lon,res):
#    return np.mean(pucher_forestcoverscaled[:,return_pucher_mask(lat,lon,res,lola_pucher)],axis=1)

### Poulter forest age data

#poulter=nc.Dataset('C:\\forest_age\\GFAD_V1-1\\GFAD_V1-1.nc')

#def get_nearest_poulterdata(lon,lat,plons,plats):
#    ### get Poulter et al. forest age distribution for given coordinates (nearest grid cell)
#    poulteragecl= np.sum(poulter["age"][:,:,np.where(abs(plats-lat)==np.min(abs(plats-lat)))[0][0],np.where(abs(plons-#lon)==np.min(abs(plons-lon)))[0][0]],axis=1)
#    poulteragecl=[np.sum(poulteragecl[(i*2):((i+1)*2)]) for i in range(8)]
#    if np.sum(poulteragecl)==0:
#        return [0.,0.,0.,0.,0.,0.,0.,1.]
#    else:
#        return poulteragecl


####### keep shrubs/grassland separate

def hilda2lpjg_1slice_shrub0(slice1):
    ###converts Hilda+ codes to numbers from 1-11, keeps grass/shrubs & other land from Hilda+ in separate category (0) in order to calculate forest age without grass/shrubs part (is normally put to NATURAL)
    slc=np.empty_like(slice1)
    ####  -classes-   ####
    #  1 -  URBAN
    #  2 -  CROPLAND
    #  3 -  PASTURE
    #  4 -  FOREST
    #  5 -  NATURAL
    #  6 -  BARREN
    #  7-11 MANAGED FORESTS
    slc[slice1<=33]=slice1[slice1<=33]/11
    slc[slice1==0]=6
    slc[slice1==66]=0
    slc[slice1==55]=0 
    slc[slice1==77]=6
    slc[(slice1>=40)*(slice1<=45)]=slice1[(slice1>=40)*(slice1<=45)]
    slc[slice1>=400]=slice1[slice1>=400]/10
    slc[slice1==45]=40
    #slc[slice1==450]=40
    slc[slc>=40]=slc[slc>=40]-33
    return slc

def shrubs2nat(slice1):
    ###converts separatet shrub/grassland pixels (0) back to NATURAL (5)
    slc=np.copy(slice1)
    slc[slice1==0]=5
    return slc


def agestr_natural(dist_int):
    ### estimate age class distribution from natural disturbance interval
    agestr=[]
    for n_y in np.arange(500,1000,10):
        ages=np.zeros(dist_int)
        for y in range(n_y):
            ages+=1
            ages[np.random.randint(dist_int)]=0
        agestr.append(np.histogram(ages,bins=[0,20,40,60,80,100,120,140,2000])[0])
    return np.sum(agestr,axis=0)/np.sum(agestr)

def allforest2Fpnv(slice1):
    ### classify all managed forests as 7 (Fpnv)
    slc=np.copy(slice1)
    slc[slice1>7]=7
    return slc


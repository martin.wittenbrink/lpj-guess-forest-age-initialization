import pandas as pd
import numpy as np

def hilda2lpjg_1slice(slice1):
    ####  transforms Hilda+ codes into codes for LPJ-GUESS LC/standtypes
    slc=np.empty_like(slice1)
    ####  -classes-   ####
    #  1 - URBAN
    #  2 - CROPLAND
    #  3 - PASTURE
    #  4 - FOREST
    #  5 - NATURAL
    #  6 - BARREN
    #  7 - Forest PNV
    #  8 - Forest NE
    #  9 - Forest BE
    # 10 - Forest ND
    # 11 - Forest BD
    slc[slice1<=33]=slice1[slice1<=33]/11
    slc[slice1==0]=6
    slc[slice1==66]=5
    slc[slice1==55]=5
    slc[slice1==77]=6
    #slc[(slice1>=40)*(slice1<=45)]=slice1[(slice1>=40)*(slice1<=45)]
    slc[(slice1>=40)*(slice1<=45)]=5
    slc[slice1>=400]=slice1[slice1>=400]/10
    slc[slice1==450]=40
    slc[slc>=40]=slc[slc>=40]-33
    return slc
    
def create_grosstrans_table(slice1,hildayear0,lon,lat):
    ### create a gross transition pandas dataframe for 1 gridcell based on extracted hilda+ data ("slice1")
    ### in this case only one crop stand type ("Crops")
    labels=["none","Urban","Crops","Pasture","Forest","Natural","Barren","Fpnv","Fne","Fbe","Fnd","Fbd"]
    grosstrans_standtypes=pd.DataFrame()
    grosstrans_standtypes["Lon"]=np.repeat(lon,slice1.shape[0]-1)
    grosstrans_standtypes["Lat"]=np.repeat(lat,slice1.shape[0]-1)
    grosstrans_standtypes["Year"]=np.arange(slice1.shape[0]-1)+hildayear0
    for i1 in [1,2,3,5,6,7,8,9,10,11]:  ### loop over standtypes
            for i2 in [1,2,3,5,6,7,8,9,10,11]:
                if i1!=i2:
                    grosstrans_standtypes[f'{labels[i1]}_{labels[i2]}']=np.mean((slice1[:-1]==i1)*(slice1[1:]==i2),axis=(1,2))
    return grosstrans_standtypes


def create_net_table(slice1,hildayear0,lon,lat,grosstable):
    ### create a net st fractions table (pandas dataframe) based on a gross trans table pd.dataframe
    net_standtypes=pd.DataFrame()
    net_standtypes["Lon"]=np.repeat(lon,slice1.shape[0])
    net_standtypes["Lat"]=np.repeat(lat,slice1.shape[0])
    net_standtypes["Year"]=np.arange(slice1.shape[0])+hildayear0
    labels=["none","Urban","Crops","Pasture","Forest","Natural","Barren","Fpnv","Fne","Fbe","Fnd","Fbd"]
    for i1 in [1,2,3,5,6,7,8,9,10,11]:  ## loop over standtypes
        netts=np.repeat(np.mean(slice1[0,:,:]==i1),slice1.shape[0]) ### set net fractions to net value of first year
        inc=[]  ### collect gross transitions to resp. standtype (increases)
        for gc in grosstable.columns[3:]:
            if gc[(gc.find("_")+1):]==labels[i1]:
                inc.append(grosstable[gc])
        if len(inc)>0:
            netts[1:]+=np.cumsum(np.sum(inc,axis=0))
        dec=[]   ### collect gross transitions from resp. standtypes (decreases)
        for gc in grosstable.columns[3:]:
            if gc[:gc.find("_")]==labels[i1]:
                dec.append(grosstable[gc])
        if len(dec)>0:
            netts[1:]-=np.cumsum(np.sum(dec,axis=0))
        netts[netts<0.]=0.        #set net fractions below 0 (due to machine precision errors) to 0.
        net_standtypes[labels[i1]]=netts
    return net_standtypes
    
 
    
def create_grosstrans_table_mirca(slice1,hildayear0,lon,lat,mirca):
    labels=["none","Urban","Crops","Pasture","Forest","Natural","Barren","Fpnv","Fne","Fbe","Fnd","Fbd"]
    grosstrans_standtypes=pd.DataFrame()
    grosstrans_standtypes["Lon"]=np.repeat(lon,slice1.shape[0]-1)
    grosstrans_standtypes["Lat"]=np.repeat(lat,slice1.shape[0]-1)
    grosstrans_standtypes["Year"]=np.arange(slice1.shape[0]-1)+hildayear0
    cropfracs=mirca.loc[np.array(mirca["Lon"]==lon)*np.array(mirca["Lat"]==lat)]
    for i1 in [1,2,3,5,7,8,9,10,11]:
            for i2 in [1,2,3,5,7,8,9,10,11]:
                if i1!=i2:
                    if i1==2:
                        croptrans=np.mean((slice1[:-1]==i1)*(slice1[1:]==i2),axis=(1,2)) # get transition from i1 to i2
                        for croptype in mirca.columns[2:]:
                            grosstrans_standtypes[f'{croptype}_{labels[i2]}']=croptrans*cropfracs[croptype].values[0]
                    elif i2==2:
                        croptrans=np.mean((slice1[:-1]==i1)*(slice1[1:]==i2),axis=(1,2)) # get transition from i1 to i2
                        for croptype in mirca.columns[2:]:
                            grosstrans_standtypes[f'{labels[i1]}_{croptype}']=croptrans*cropfracs[croptype].values[0]                
                    else:
                        grosstrans_standtypes=grosstrans_standtypes.copy()
                        grosstrans_standtypes[f'{labels[i1]}_{labels[i2]}']=np.mean((slice1[:-1]==i1)*(slice1[1:]==i2),axis=(1,2))
    return grosstrans_standtypes
    
    
def create_net_table_mirca(slice1,hildayear0,lon,lat,grosstable,mirca):
    net_standtypes=pd.DataFrame()
    net_standtypes["Lon"]=np.repeat(lon,slice1.shape[0])
    net_standtypes["Lat"]=np.repeat(lat,slice1.shape[0])
    net_standtypes["Year"]=np.arange(slice1.shape[0])+hildayear0
    labels=["none","Urban","Crops","Pasture","Forest","Natural","Barren","Fpnv","Fne","Fbe","Fnd","Fbd"]
    cropfracs=mirca.loc[np.array(mirca["Lon"]==lon)*np.array(mirca["Lat"]==lat)]
    for i1 in [1,3,5,6,7,8,9,10,11]:  ### loop over all standtypes except crops
        netts=np.repeat(np.mean(slice1[0,:,:]==i1),slice1.shape[0]) ### set net fractions to net value of first year
        inc=[]    ### collect gross transitions to resp. standtype (increases)
        for gc in grosstable.columns[3:]:
            if gc[(gc.find("_")+1):]==labels[i1]:
                inc.append(grosstable[gc])
        if len(inc)>0:
            netts[1:]+=np.cumsum(np.sum(inc,axis=0))
        dec=[]   ### collect gross transitions from resp. standtypes (decreases)
        for gc in grosstable.columns[3:]:
            if gc[:gc.find("_")]==labels[i1]:
                dec.append(grosstable[gc])
        if len(dec)>0:
            netts[1:]-=np.cumsum(np.sum(dec,axis=0))
        netts[netts<0.]=0.        #set net fractions below 0 (due to machine precision errors) to 0.
        net_standtypes[labels[i1]]=netts       
    ### handle splijtting crops to mirca fractions
    i1=2
    netts_crop=np.repeat(np.mean(slice1[0,:,:]==i1),slice1.shape[0])
    for croptype in mirca.columns[2:]:
        netts=netts_crop*cropfracs[croptype].values[0]
        inc=[]
        for gc in grosstable.columns[3:]:
            if gc[(gc.find("_")+1):]==croptype:
                inc.append(grosstable[gc])
        if len(inc)>0:
            netts[1:]+=np.cumsum(np.sum(inc,axis=0))
        dec=[]
        for gc in grosstable.columns[3:]:
            if gc[:gc.find("_")]==croptype:
                dec.append(grosstable[gc])
        if len(dec)>0:
            netts[1:]-=np.cumsum(np.sum(dec,axis=0))
        netts[netts<0.]=0.
        net_standtypes[croptype]=netts        
    return net_standtypes


def create_net_table_mirca_smallcropfrac(slice1,hildayear0,lon,lat,grosstable,mirca):
    smallcropfrac=1e-7
    fromlc=0
    if any(np.mean(slice1==2,axis=(1,2))==0)*any(np.mean(slice1==2,axis=(1,2))>0):    ### check if crops are always zero or always non-zero
        for i1 in [11,10,9,8,7,3,1,5,6]:                                              ### find lc that never below smallcropfrac in (net) 
            if np.min(np.mean(slice1==i1,axis=(1,2)))>smallcropfrac:
                fromlc=i1
    if fromlc==0:         ### if no lc stays always above smallcropfrac, we don't add the smallcropfrac to net crop fractions
        smallcropfrac=0.
    net_standtypes=pd.DataFrame()
    net_standtypes["Lon"]=np.repeat(lon,slice1.shape[0])
    net_standtypes["Lat"]=np.repeat(lat,slice1.shape[0])
    net_standtypes["Year"]=np.arange(slice1.shape[0])+hildayear0
    labels=["none","Urban","Crops","Pasture","Forest","Natural","Barren","Fpnv","Fne","Fbe","Fnd","Fbd"]
    cropfracs=mirca.loc[np.array(mirca["Lon"]==lon)*np.array(mirca["Lat"]==lat)]
    for i1 in [1,3,5,6,7,8,9,10,11]:
        netts=np.repeat(np.mean(slice1[0,:,:]==i1),slice1.shape[0])
        if i1 == fromlc:
            netts-=smallcropfrac
        inc=[]
        for gc in grosstable.columns[3:]:
            if gc[(gc.find("_")+1):]==labels[i1]:
                inc.append(grosstable[gc])
        if len(inc)>0:
            netts[1:]+=np.cumsum(np.sum(inc,axis=0))
        dec=[]
        for gc in grosstable.columns[3:]:
            if gc[:gc.find("_")]==labels[i1]:
                dec.append(grosstable[gc])
        if len(dec)>0:
            netts[1:]-=np.cumsum(np.sum(dec,axis=0))
        netts[netts<0.]=0.
        net_standtypes[labels[i1]]=netts
    ###crops
    i1=2
    netts_crop=np.repeat(np.mean(slice1[0,:,:]==i1),slice1.shape[0])+smallcropfrac
    for croptype in mirca.columns[2:]:
        netts=netts_crop*cropfracs[croptype].values[0]
        inc=[]
        for gc in grosstable.columns[3:]:
            if gc[(gc.find("_")+1):]==croptype:
                inc.append(grosstable[gc])
        if len(inc)>0:
            netts[1:]+=np.cumsum(np.sum(inc,axis=0))
        dec=[]
        for gc in grosstable.columns[3:]:
            if gc[:gc.find("_")]==croptype:
                dec.append(grosstable[gc])
        if len(dec)>0:
            netts[1:]-=np.cumsum(np.sum(dec,axis=0))
        netts[netts<0.]=0.
        net_standtypes[croptype]=netts        
    return net_standtypes        
    


def add_tab( src_filename):
    ### fucntion to add a "/t" and "/n" to header line of txt input files in order to make LPJ-GUESS read them in properly
    f = open(src_filename)
    first_line, remainder = f.readline(), f.read()
    f.close()
    t = open(src_filename,"w")
    t.write(first_line[:-1] + "\t\n")
    t.write(remainder)
    t.close()

def write_output_txt(df1,filename):
    ### function to write txt input file from pandas dataframe
    df1.to_csv(filename,index=False,sep=" ")
    add_tab(filename)

def make_netlandcover(net_st):
    ### create net landcover pd.dataframe from net standtypes fractions
    netlc=net_st[net_st.columns[:3]].copy()
    labels=["Urban","Crops","Pasture","Natural","Barren"]
    lcnames=["URBAN","CROPS","PASTURE","NATURAL","BARREN"]
    for lc in range(len(labels)):
        netlc[lcnames[lc]]=net_st[labels[lc]].copy()
    netlc["FOREST"]=np.sum(net_st[["Fbd","Fbe","Fpnv","Fne","Fnd"]],axis=1)
    return netlc
    
    
def make_netlandcover_mirca(net_st,mirca):
    ### create net landcover pd.dataframe from net standtypes fractions
    netlc=net_st[net_st.columns[:3]].copy()
    labels=["Urban","Pasture","Natural","Barren"]
    lcnames=["URBAN","PASTURE","NATURAL","BARREN"]
    for lc in range(len(labels)):
        netlc[lcnames[lc]]=net_st[labels[lc]].copy()
    netlc["FOREST"]=np.sum(net_st[["Fbd","Fbe","Fpnv","Fne","Fnd"]],axis=1)
    netlc["CROPLAND"]=np.sum(net_st[mirca.columns[2:]],axis=1)
    return netlc

def make_netlandcover_mirca_forestfracs(net_st,mirca,forestfracs):
    ### create net landcover pd.dataframe from net standtypes fractions
    netlc=net_st[net_st.columns[:3]].copy()
    labels=["Urban","Pasture","Natural","Barren"]
    lcnames=["URBAN","PASTURE","NATURAL","BARREN"]
    for lc in range(len(labels)):
        netlc[lcnames[lc]]=net_st[labels[lc]].copy()
    netlc["FOREST"]=np.sum(net_st[forestfracs.columns[2:]],axis=1)
    netlc["CROPLAND"]=np.sum(net_st[mirca.columns[2:]],axis=1)
    return netlc

    
def make_luforest(net_st):
    ### create net forest st fractions pd.dataframe from net standtypes fractions
    netluf=net_st[net_st.columns[:3]].copy()
    for label in ["Fbd","Fbe","Fpnv","Fne","Fnd"]:
        netluf[label]=net_st[label].copy()
    return netluf

def make_luforest_forestfracs(net_st,forestfracs):
    ### create net forest st fractions pd.dataframe from net standtypes fractions
    netluf=net_st[net_st.columns[:3]].copy()
    for label in forestfracs.columns[2:]:
        netluf[label]=net_st[label].copy()
    return netluf
                                  
                                  
def make_lucrop_mirca(net_st,mirca):
    ### create net crop st fractions pd.dataframe from net standtypes fractions
    netluc=net_st[net_st.columns[:3]].copy()
    for label in mirca.columns[2:]:
        netluc[label]=net_st[label].copy()
    return netluc
  
    
def hilda2lpjg_1slice_shrub0(slice1):
    slc=np.empty_like(slice1)
    ####  -classes-   ####
    #  1 - URBAN
    #  2 - CROPLAND
    #  3 - PASTURE
    #  4 - FOREST
    #  5 - NATURAL
    #  6 - BARREN
    slc[slice1<=33]=slice1[slice1<=33]/11
    slc[slice1==0]=6
    slc[slice1==66]=0
    slc[slice1==55]=0 
    slc[slice1==77]=6
    slc[(slice1>=40)*(slice1<=45)]=slice1[(slice1>=40)*(slice1<=45)]
    slc[slice1>=400]=slice1[slice1>=400]/10
    slc[slice1==45]=40
    #slc[slice1==450]=40
    slc[slc>=40]=slc[slc>=40]-33
    return slc

def shrubs2nat(slice1):
    slc=np.copy(slice1)
    slc[slice1==0]=5
    return slc


def create_grosstrans_n_netst_table_mirca_forestfracs(slice1,hildayear0,lon,lat,mirca,forestfracs):
    labels=["none","Urban","Crops","Pasture","Forest","Natural","Barren","FOREST"]
    grosstrans_standtypes=pd.DataFrame()
    grosstrans_standtypes["Lon"]=np.repeat(lon,slice1.shape[0]-1)
    grosstrans_standtypes["Lat"]=np.repeat(lat,slice1.shape[0]-1)
    grosstrans_standtypes["Year"]=np.arange(slice1.shape[0]-1)+hildayear0
    net_firstyear=pd.DataFrame({"Lon": [lon], "Lat":[lat],"Year":[hildayear0]})
    cropfracs=mirca.loc[np.array(mirca["Lon"]==lon)*np.array(mirca["Lat"]==lat)]
    forfracs=forestfracs.loc[np.array(forestfracs["Lon"]==lon)*np.array(forestfracs["Lat"]==lat)]
    for i1 in [1,2,3,5,6,7]:
            for i2 in [1,2,3,5,6,7]:
                if i1==i2:###write net_firstyear
                    net_fy=np.mean(slice1[0]==i1)
                    if i1==2:
                        for croptype in mirca.columns[2:]:
                            net_firstyear[croptype]=[cropfracs[croptype].values[0]*net_fy]
                    elif i1==7:
                        for foresttype in forestfracs.columns[2:]:
                            net_firstyear[foresttype]=[forfracs[foresttype].values[0]*net_fy]
                    else:
                        net_firstyear[labels[i1]]=net_fy
                elif (i1!=6)*(i2!=6):###write gross table (no barren)
                    transition=np.mean((slice1[:-1]==i1)*(slice1[1:]==i2),axis=(1,2)) # get transition from i1 to i2
                    if i1==2:
                        if i2!=7:
                            grosstrans_standtypes=grosstrans_standtypes.copy()
                            for croptype in mirca.columns[2:]:
                                grosstrans_standtypes[f'{croptype}_{labels[i2]}']=transition*cropfracs[croptype].values[0]
                            grosstrans_standtypes=grosstrans_standtypes.copy()
                        else:
                            grosstrans_standtypes=grosstrans_standtypes.copy()
                            for croptype in mirca.columns[2:]:
                                for foresttype in forestfracs.columns[2:]:
                                    grosstrans_standtypes[f'{croptype}_{foresttype}'] = transition*cropfracs[croptype].values[0]*forfracs[foresttype].values[0]
                    elif i2==2:
                        if i1!=7:
                            for croptype in mirca.columns[2:]:
                                grosstrans_standtypes=grosstrans_standtypes.copy()
                                grosstrans_standtypes[f'{labels[i1]}_{croptype}']=transition*cropfracs[croptype].values[0]
                        else:
                            grosstrans_standtypes=grosstrans_standtypes.copy()
                            for croptype in mirca.columns[2:]:
                                for foresttype in forestfracs.columns[2:]:
                                    grosstrans_standtypes[f'{foresttype}_{croptype}']=transition*cropfracs[croptype].values[0]*forfracs[foresttype].values[0]
                    else:
                        grosstrans_standtypes=grosstrans_standtypes.copy()                        
                        if i1==7:
                            for foresttype in forestfracs.columns[2:]:
                                grosstrans_standtypes[f'{foresttype}_{labels[i2]}']=transition*forfracs[foresttype].values[0]
                        elif i2==7:
                            for foresttype in forestfracs.columns[2:]:
                                grosstrans_standtypes[f'{labels[i1]}_{foresttype}']=transition*forfracs[foresttype].values[0] 
                        else:
                            grosstrans_standtypes=grosstrans_standtypes.copy()
                            grosstrans_standtypes[f'{labels[i1]}_{labels[i2]}']=transition
    return grosstrans_standtypes, net_firstyear

def create_net_table_auto(grosstable,net_firstyear):
    net_standtypes=pd.DataFrame()
    net_standtypes["Lon"]=np.array(grosstable["Lon"])
    net_standtypes["Lat"]=np.array(grosstable["Lat"])
    net_standtypes["Year"]=np.array(grosstable["Year"])+1
    standtypes=net_firstyear.columns[3:]
    for standtype in standtypes:  ### loop over all standtypes except crops & forest
        netts=np.repeat(net_firstyear[standtype][0],len(np.array(grosstable["Year"]))) ### set net fractions to net value of first year
        inc=[]    ### collect gross transitions to resp. standtype (increases)
        for gc in grosstable.columns[3:]:
            if gc[(gc.find("_")+1):]==standtype:
                inc.append(grosstable[gc])
        if len(inc)>0:
            netts+=np.cumsum(np.sum(inc,axis=0))
        dec=[]   ### collect gross transitions from resp. standtypes (decreases)
        for gc in grosstable.columns[3:]:
            if gc[:gc.find("_")]==standtype:
                dec.append(grosstable[gc])
        if len(dec)>0:
            decsum=np.sum(dec,axis=0)
            netts-=np.cumsum(decsum)
        #netts[netts<0.]=0.
        net_standtypes[standtype]=netts
    return pd.concat([net_firstyear,net_standtypes])

def round_dataframe(df,deci=3):
    ### round dataframe data columns to specified number of decimals, make sure all data columns add up to 1 everywhere
    cc=2
    if df.columns[2]=="Year":
        cc=3
    if df.columns[2]=="year":
        cc=3
    df2=df[df.columns[cc:]].copy()
    df2=np.round(np.array(df2),deci)
    sums=np.sum(df2,axis=1)-1.
    ama=np.argmax(df2,axis=1)
    for i in range(df2.shape[0]):
        if sums[i]!=0.:
            df2[i,ama[i]]-=sums[i]
    dfnew=df[df.columns[:cc]].copy()
    for j in range(df2.shape[1]):
        dfnew[df.columns[j+2]]=df2[:,j]
    return dfnew